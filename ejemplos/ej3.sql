﻿USE programacion;

-- 2 Area triangulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej2(base float, altura float) RETURNS float
  BEGIN
    RETURN (base*altura)/2;
  END //
  DELIMITER;

-- 3 Perimetro triangulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej3(base float, lado2 float, lado3 float) RETURNS float
  BEGIN
    RETURN base+lado2+lado3;
  END //
  DELIMITER;

-- 4 Actualizar area y perimetro de los triangulos en la tabla comprendidos entre dos ids indicadas
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4(id1 int, id2 int)
  BEGIN
    UPDATE triangulos set
      area=ej2(base,altura),
      perimetro=ej3(base,lado2,lado3)
      WHERE id BETWEEN id1 AND id2;
  END //
  DELIMITER ;
  CALL ej4(1,19);

-- 5 funcion area cuadrado
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej5 (lado float) RETURNS float
  BEGIN
    RETURN POW(lado,2);
  END //            
  DELIMITER ;

-- 6 funcion perimetro cuadrado
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej6 (lado float) RETURNS float
  BEGIN
    RETURN lado*4; 
  END //
  DELIMITER ;

-- 7  Actualizar area y perimetro de los cuadrados en la tabla comprendidos entre dos ids indicadas
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7 (id1 int, id2 int)
  BEGIN
    UPDATE cuadrados c set
      c.area=ej5(c.lado),
      c.perimetro=ej6(c.lado)
      WHERE c.id BETWEEN id1 AND id2;
  END //
  DELIMITER ;

CALL ej7(1,19);

-- 8 funcion area rectangulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej8(lado1 float, lado2 float) RETURNS float
  BEGIN
    RETURN lado1*lado2;
  END //
  DELIMITER ;

-- 9 funcion perimetro rectangulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej9(lado1 float, lado2 float) RETURNS float
  BEGIN
    RETURN (lado1+lado2)*2;
  END //
  DELIMITER ;

-- 10 Actualizar area y perimetro de los rectangulos en la tabla comprendidos entre dos ids indicadas
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej10(id1 int, id2 int)
  BEGIN
    UPDATE rectangulo r set
      r.area=ej8(r.base,r.altura),
      r.perimetro=ej9(r.base,r.altura)
      WHERE id BETWEEN id1 AND id2;     
  END //
  DELIMITER ;
CALL ej10(1,19);

-- 11 funcion area circulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej11(radio float) RETURNS float
  BEGIN
    RETURN PI()*POW(radio,2); 
  END //
  DELIMITER ;

-- 12 funcion perimetro circulo
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej12(radio float) RETURNS float
  BEGIN
    RETURN 2*PI()*radio; 
  END //
  DELIMITER ;

/* 13 Actualizar area y perimetro de los circulo en la tabla comprendidos entre 
  dos ids indicadas y que sean del tipo pasado*/
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej13(id1 int, id2 int, tipo varchar(4))
  BEGIN
     IF tipo = 'a' OR tipo = 'b' THEN    
       UPDATE circulos c set
         c.area=ej11(c.radio),
         c.perimetro=ej12(c.radio)
         WHERE c.id BETWEEN id1 AND id2             
         AND c.tipo=tipo;
     ELSE
       UPDATE circulos c set
         c.area=ej11(c.radio),
         c.perimetro=ej12(c.radio)
         WHERE c.id BETWEEN id1 AND id2; 
     END IF;
  END //
  DELIMITER ;

  CALL ej13(1,17,'b');
  SELECT * FROM circulos c;

-- 14 Funcion media de 4 notas
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej14(n1 float, n2 float, n3 float, n4 float) RETURNS float
  BEGIN
     RETURN (n1+n2+n3+n4)/4;
  END //
  DELIMITER ;

-- 15 Funcion minimo de 4 notas
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej15(n1 float, n2 float, n3 float, n4 float) RETURNS float
  BEGIN
     RETURN LEAST(n1,n2,n3,n4);
  END //
  DELIMITER ;

-- 16 Funcion maximo de 4 notas
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej16(n1 float, n2 float, n3 float, n4 float) RETURNS float
  BEGIN
     RETURN GREATEST(n1,n2,n3,n4);
  END //
  DELIMITER ;

-- 17 Funcion moda de 4 notas
  DELIMITER //
  CREATE OR REPLACE FUNCTION ej17(n1 float, n2 float, n3 float, n4 float) RETURNS float
  BEGIN
     DECLARE moda float;
     CREATE OR REPLACE TEMPORARY TABLE moda (nota float);
     INSERT INTO moda
       VALUES (n1),(n2), (n3), (n4);
     SELECT nota INTO moda FROM moda GROUP BY nota ORDER BY COUNT(*) DESC LIMIT 1;        
     RETURN moda;
  END //
  DELIMITER ;

/* 18 Actualizar notas minima, maxima, media y moda de alumnos entre los ids indicados.
   Actualizar tambien tabla grupos colocando la nota media de los alumnos entre los ids indicados. */
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej18(id1 int, id2 int)
  BEGIN
    UPDATE alumnos a set
      media=ej14(a.nota1,a.nota2,a.nota3,a.nota4),
      a.min=ej15(a.nota1,a.nota2,a.nota3,a.nota4),
      a.max=ej16(a.nota1,a.nota2,a.nota3,a.nota4),
      a.moda=ej17(a.nota1,a.nota2,a.nota3,a.nota4)
      WHERE id BETWEEN id1 AND id2;

    UPDATE grupos g set 
      g.media= (SELECT ROUND(AVG(a.media),2) FROM alumnos a where a.id BETWEEN id1 AND id2 
         AND a.grupo=1 GROUP BY a.grupo)
      WHERE g.id=1;

    UPDATE grupos g set 
      g.media= (SELECT ROUND(AVG(a.media),2) FROM alumnos a where a.id BETWEEN id1 AND id2 
         AND a.grupo=2 GROUP BY a.grupo)
      WHERE g.id=2;

    SELECT * FROM alumnos a;
    SELECT * FROM grupos g;
  END //
  DELIMITER ;

  CALL ej18(1,20);

SELECT AVG(a.media), a.grupo FROM alumnos a where a.id BETWEEN 1 AND 5
         AND a.grupo=2 GROUP BY a.grupo;