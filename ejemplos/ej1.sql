﻿DROP DATABASE IF EXISTS ej1proc;
CREATE DATABASE ej1proc;
USE ej1proc;

-- Ej1 Dos numeros e indique cual es mayor
  -- con IF
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej1if(n1 int, n2 int)
      BEGIN
       DECLARE mayor int DEFAULT 0;
       IF n1>n2 THEN
        SET mayor=n1;
       ELSE 
        SET mayor=n2;
       END IF;    
       SELECT mayor;
      END //
    DELIMITER ;
    CALL ej1if(5,7);

  -- Con consulta de totales
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej1tot(n1 int, n2 int)
      BEGIN
       CREATE OR REPLACE TEMPORARY TABLE datos (
        id int AUTO_INCREMENT PRIMARY KEY,
        num int DEFAULT 0
       );
       INSERT INTO datos(num)
        VALUES (n1),(n2);
       SELECT MAX(num)FROM datos d;

      END //
    DELIMITER ;
    CALL ej1tot(5,10);

  -- Con funcion MySQL
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej1fun(n1 int, n2 int)
      BEGIN
        SELECT GREATEST(n1,n2);
      END //
    DELIMITER ;
    CALL ej1fun(8,5);

-- Ej2 Tres numeros e indique cual es menor
  -- con IF
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej2if(n1 int, n2 int, n3 int)
      BEGIN
       DECLARE mayor int DEFAULT 0;

       IF n1>n2 THEN
        SET mayor=n1;
       ELSE
        SET mayor=n2;
       END IF;

       IF mayor<n3 THEN 
        SET mayor=n3;
       END IF;    

       SELECT mayor;
      END //
    DELIMITER ;
    CALL ej2if(11,7,5);

  -- Con consulta de totales
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej2tot(n1 int, n2 int, n3 int)
      BEGIN 
       CREATE OR REPLACE TEMPORARY TABLE datos (
        id int AUTO_INCREMENT PRIMARY KEY,
        num int DEFAULT 0
       );
       INSERT INTO datos(num)
        VALUES (n1),(n2);
       SELECT MAX(num)FROM datos d;

      END //
    DELIMITER ;
    CALL ej1tot(5,10);

  -- Con funcion MySQL
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ej1fun(n1 int, n2 int, n3 int)
      BEGIN
        SELECT GREATEST(n1,n2, n3);
      END //
    DELIMITER ;
    CALL ej1fun(8,5,2);

-- Ej3 Tres numeros e indique el mayor y el menor con argumentos de salida
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej3(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
    BEGIN
     
     IF n1>n2 THEN
      SET mayor=n1;
      SET menor=n2;
     ELSE
      SET mayor=n2;
      SET menor=n1;
     END IF;

     IF mayor<n3 THEN 
      SET mayor=n3;
     ELSEIF menor>n3 THEN    
      SET menor=n3;
     END IF;    

    END //
  DELIMITER ;

  CALL ej3(4,2,3,@mayor, @menor);
  SELECT @mayor, @menor;

-- Ej4 2 fechas y mostrar la diferencia de dias
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4 (f1 date, f2 date)
    BEGIN
      SELECT DATEDIFF(f2,f1);
    END //
  DELIMITER ;
  
  CALL ej4('1989-02-23', '2019-06-16');

-- Ej5 2 fechas y mostrar la diferencia de meses
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej5 (f1 date, f2 date)
    BEGIN
      SELECT TIMESTAMPDIFF(MONTH,f1,f2);
    END //
  DELIMITER ;
  
  CALL ej5('1989-02-23', '2019-06-16');

-- Ej6 2 fechas y devolver dias, meses y años entre las dos como argumentos de salida
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6 (f1 date, f2 date, OUT dias int, OUT meses int, OUT años int)
    BEGIN
      SET dias=TIMESTAMPDIFF(DAY,f1,f2);      
      SET meses=TIMESTAMPDIFF(MONTH,f1,f2);
      SET años=TIMESTAMPDIFF(YEAR,f1,f2);
    END //
  DELIMITER ;
  
  CALL ej6('1989-02-23', '2019-06-16', @dias, @meses, @años);
  SELECT @dias, @meses, @años;
 
-- Ej7 Contar caracteres de una frase
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7 (frase varchar(20))
    BEGIN
      SELECT CHAR_LENGTH(frase);
    END //
  DELIMITER ;
  CALL ej7('Holaaaaaaaaa');
