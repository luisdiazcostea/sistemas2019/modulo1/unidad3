﻿DROP DATABASE IF EXISTS ejemplo6;
CREATE DATABASE ejemplo6;
USE ejemplo6;

CREATE OR REPLACE TABLE ventas (
  id int(11) NOT NULL AUTO_INCREMENT,
  producto varchar(5) DEFAULT NULL,
  precio int(11) NOT NULL,
  unidades int(11) NOT NULL,
  total int(11) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO ventas VALUES
(1, 'p1', 10, 5, 50),
(2, 'p2', 20, 3, 60),
(3, 'p1', 10, 7, 70),
(4, 'p3', 30, 7, 210),
(5, 'p1', 10, 8, 80),
(6, 'p1', 10, 9, 90),
(7, 'p2', 20, 10, 200),
(8, 'p1', 10, 11, 110),
(9, 'p4', 30, 12, 360),
(10, 'p1', 10, 13, 130),
(11, 'p2', 20, 14, 280),
(12, 'p3', 30, 15, 450),
(13, 'p4', 30, 16, 480),
(14, 'p5', 30, 17, 510);

CREATE OR REPLACE TABLE productos(
  producto varchar(5),
  cantidad int DEFAULT 0,
  PRIMARY KEY(producto)
  );

INSERT INTO productos (producto)
  VALUES ('p1'),('p2'),('p3'),('p4'),('p5');



/* 1 Disparador para la tabla ventas que al insertar calcule el total */
DELIMITER //
CREATE OR REPLACE TRIGGER ventasBI
  BEFORE INSERT ON ventas FOR EACH ROW
  BEGIN
    SET new.total=new.precio*new.unidades;
  END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p3', 30, 20);

/* 2 Disparador para la tabla ventas que al insertar sume el total en la tabla productos */
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAI
  AFTER INSERT ON ventas FOR EACH ROW
  BEGIN
    UPDATE productos 
      SET cantidad=NEW.unidades+cantidad
      WHERE producto=NEW.producto;
  END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p5', 50, 2);

/* 3  Disparador para la tabla ventas que al actualizar calcule el total */
  DELIMITER //
CREATE OR REPLACE TRIGGER ventasBU
  BEFORE UPDATE ON ventas FOR EACH ROW
  BEGIN
    SET new.total=new.precio*new.unidades;
  END //
DELIMITER ;

UPDATE ventas v
  SET v.precio=12
  WHERE v.id=1;

/* 4 Disparador para la tabla ventas que al actualicar sume el total en la tabla productos */
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAU
  AFTER UPDATE ON ventas FOR EACH ROW
  BEGIN
    UPDATE productos 
      SET cantidad=NEW.unidades+cantidad
      WHERE producto=NEW.producto;
  END //
DELIMITER ;

UPDATE ventas v
  SET v.unidades=7
  WHERE v.id=1;

/* 5 Disparador para la tabla productos que si se cambia el codigo de producto sume todos los 
  totales a ese producto de la tabla ventas */

DELIMITER//
CREATE OR REPLACE TRIGGER productosBU
  BEFORE UPDATE ON productos FOR EACH ROW
  BEGIN
   SET new.cantidad=(SELECT SUM(v.unidades) FROM ventas v
                      WHERE v.producto=new.producto);
  END //
DELIMITER ;
 

/* 6 Disparador para productos que si eliminas un producto elimine los mismo productos de ventas */
DELIMITER //
CREATE OR REPLACE TRIGGER productosBD 
  BEFORE DELETE ON productos FOR EACH ROW
  BEGIN
    DELETE FROM ventas
      WHERE producto=OLD.producto;    
  END //
DELIMITER ;

DELETE FROM productos WHERE producto='p3';

/* 7 Disparador para ventas que reste el total del campo cantidad de productos al borrar una venta */
DELIMITER //
CREATE OR REPLACE TRIGGER ventasAD
  after DELETE ON ventas FOR EACH ROW
  BEGIN
   UPDATE productos p
    SET p.cantidad=p.cantidad-OLD.unidades
    WHERE p.producto=OLD.producto;
  END //
DELIMITER ;

DELETE FROM ventas WHERE id=16;
