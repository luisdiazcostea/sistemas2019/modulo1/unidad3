﻿DROP DATABASE IF EXISTS ejemplo1;
CREATE DATABASE ejemplo1;
USE ejemplo1;


-- 1 Procedimiento que recibe dos numeros e indica cual es mayor
  -- if
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej1a (num1 int, num2 int)
        BEGIN
          DECLARE n1 int DEFAULT 0;
          DECLARE n2 int DEFAULT 0;
          SET n1=num1;
          SET n2=num2;
          IF n1>n2 THEN
            SELECT n1;
          ELSE
            SELECT n2;
          END IF;
        END //
    DELIMITER ;
    
    CALL ej1a(8,5);
  
  -- consulta de totales
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej1b (num1 int, num2 int)
        BEGIN
          CREATE OR REPLACE TEMPORARY TABLE numeros(
            num int);
          INSERT INTO numeros (num) VALUE (num1);
          INSERT INTO numeros (num) VALUE (num2);
          SELECT MAX(num) FROM numeros n;
        END //
    DELIMITER ;
    
    CALL ej1b(8,50);

  -- funcion mysql
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej1c (num1 int, num2 int)
        BEGIN
          SELECT GREATEST(num1, num2);
        END //
    DELIMITER ;
    
    CALL ej1c(8,50);

-- 2 Procedimiento que recibe tres numeros e indica cual es mayor
  -- if
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej2a (num1 int, num2 int, num3 int)
        BEGIN
          DECLARE n1 int DEFAULT 0;
          DECLARE n2 int DEFAULT 0;
          DECLARE n3 int DEFAULT 0;
          DECLARE max int DEFAULT 0;
          SET n1=num1;
          SET n2=num2;
          SET n3=num3;
          SET max=n1;
          IF n2>max THEN
            SET max=n2;
          ELSEIF n3>max THEN
            SET max=n3;
          END IF;
          SELECT MAX;
        END //
    DELIMITER ;
    
    CALL ej2a(6,5,6);
  
  -- consulta de totales
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej2b (num1 int, num2 int, num3 int)
        BEGIN
          CREATE OR REPLACE TEMPORARY TABLE numeros(
            num int);
          INSERT INTO numeros (num) VALUE (num1);
          INSERT INTO numeros (num) VALUE (num2);
          INSERT INTO numeros (num) VALUE (num3);
          SELECT MAX(num) FROM numeros n;
        END //
    DELIMITER ;
    
    CALL ej2b(4,5,3);

  -- funcion mysql
    DELIMITER //
      CREATE OR REPLACE PROCEDURE ej2c (num1 int, num2 int, num3 int)
        BEGIN
          SELECT GREATEST(num1, num2, num3);
        END //
    DELIMITER ;
    
    CALL ej2c(4,5,3);