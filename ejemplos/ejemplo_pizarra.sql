﻿USE programacion;
CREATE OR REPLACE TABLE salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int DEFAULT 0
);

CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
);

-- salasBI: disparador que calcule la edad de la sala en funcion de su fecha de alta
  INSERT INTO salas (butacas, fecha)
    VALUES (50,'2012/1/1');

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBI
    BEFORE INSERT 
      ON salas
      FOR EACH ROW
    BEGIN
      SET new.edad =TIMESTAMPDIFF(year,new.fecha,NOW());
    END //
  DELIMITER ;

-- salasBU: disparador que calcule la edad de la sala en funcion de su fecha de alta
  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBU
    BEFORE UPDATE 
      ON salas
      FOR EACH ROW
    BEGIN
      SET new.edad =TIMESTAMPDIFF(year,new.fecha,NOW());
    END //
  DELIMITER ;

UPDATE salas s
  SET fecha='2003/6/19' 
  WHERE id=4;
SELECT * FROM salas s;


-- Necesito que la tabla salas tenga 3 campos nuevos: dia, mes y año. Tendran el dia, mes y año de fecha automaticamente. Disparador para insertar y otro para actualizar

  ALTER TABLE salas ADD COLUMN dia int, ADD COLUMN mes int, ADD COLUMN año int;

   DELIMITER //
  CREATE OR REPLACE TRIGGER salasBI
    BEFORE INSERT 
      ON salas
      FOR EACH ROW
    BEGIN
      SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
      SET new.dia=DAY(new.fecha);
      SET new.mes=MONTH(new.fecha);
      SET new.año=year(new.fecha);
    END //
  DELIMITER ;

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBU
    BEFORE UPDATE 
      ON salas
      FOR EACH ROW
    BEGIN
      SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
      SET new.dia=DAY(new.fecha);
      SET new.mes=MONTH(new.fecha);
      SET new.año=year(new.fecha);
    END //
  DELIMITER ;


-- Calcular prefio final(precio unitario por unidad)

  CREATE OR REPLACE TABLE ventas1(
    id int AUTO_INCREMENT PRIMARY KEY,
    fecha date,
    precio_unitario int,
    unidades int, 
    pf int
  );
  INSERT INTO ventas1 (fecha, precio_unitario, unidades)
  VALUES ('2000/1/1', 34,5), ('2000/2/3',10,7);

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventas1BI
    BEFORE INSERT ON ventas1 FOR EACH ROW
    BEGIN
      SET new.pf=new.precio_unitario*new.unidades;
    END //
  DELIMITER ;

DELIMITER //
  CREATE OR REPLACE TRIGGER ventas1BU
    BEFORE UPDATE ON ventas1 FOR EACH ROW
    BEGIN
      SET new.pf=new.precio_unitario*new.unidades;
    END //
  DELIMITER ;

  INSERT INTO ventas1 (fecha, precio_unitario, unidades)
  VALUES ('2005/5/8', 12,4);

  UPDATE ventas1 
    SET precio_unitario=15
    WHERE id=1;

SELECT * FROM ventas1;