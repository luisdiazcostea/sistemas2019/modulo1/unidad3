﻿DROP DATABASE IF EXISTS ejemplotrigger;
CREATE DATABASE IF NOT EXISTS ejemplotrigger;
USE ejemplotrigger;

CREATE OR REPLACE TABLE local(
  id int PRIMARY KEY AUTO_INCREMENT,
  nombre varchar(50),
  precio float,
  plazas int
);

CREATE OR REPLACE TABLE usuario(
  id int PRIMARY KEY AUTO_INCREMENT,
  nombre varchar(50),
  inicial char(5)
);

CREATE OR REPLACE TABLE usa(
  local int,
  usuario int,
  fecha date,
  dias int,
  total float,
  PRIMARY KEY(local,usuario)  
);

-- Al introducir usuario, que calcule y almacene su inicial
  DELIMITER //
  CREATE OR REPLACE TRIGGER usuarioBI
    BEFORE INSERT ON usuario FOR EACH ROW
    BEGIN
      SET new.inicial=LEFT(new.nombre,1);
    END //

  DELIMITER ;


      /* UPDATE usuario u SET u.inicial=LEFT(nombre,1)
        WHERE u.id=new.id; */

INSERT INTO usuario(nombre)
  VALUE ('Roberto');
SELECT * FROM usuario u;

TRUNCATE usuario;
INSERT INTO usuario(nombre)
  VALUES ('Roberto'),('ana'),('kevin');

ALTER TABLE usuario ADD COLUMN contador int DEFAULT 0;
ALTER TABLE usa drop COLUMN contador;

/* al insertar un usuario en la tabla usa se debe sumar 1 a un contador */

  DELIMITER //
  CREATE OR REPLACE TRIGGER usaAI
    AFTER INSERT ON usa FOR EACH ROW
    BEGIN
      UPDATE usuario u
        SET contador=contador+1
        WHERE id=new.usuario;
    END //
  DELIMITER ;

TRUNCATE usa;
INSERT INTO usa (local, usuario)
  VALUES(1,1),(1,2),(2,1);
SELECT * FROM usuario u;
SELECT * FROM  usa u;

/* consulta de actualizacion que haga lo mismo que el trigger anterior */
  UPDATE usuario u JOIN (SELECT usuario, COUNT(*) numero FROM usa GROUP BY usuario) c1 
    ON u.id=c1.usuario
  SET contador=numero;

  SELECT usuario, COUNT(*)numero FROM usa GROUP BY usuario;