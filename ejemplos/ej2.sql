﻿DROP DATABASE ejemplo2programacion;
CREATE DATABASE ejemplo2programacion;
USE ejemplo2programacion;

CREATE TABLE datos (
 datos_id INT(11) NOT NULL AUTO_INCREMENT,
 numero1 INT(11) NOT NULL,
 numero2 INT(11) NOT NULL,
 suma VARCHAR(255) DEFAULT NULL,
 resta VARCHAR(255) DEFAULT NULL,
 rango VARCHAR(5) DEFAULT NULL,
 texto1 VARCHAR(25) DEFAULT NULL,
 texto2 VARCHAR(25) DEFAULT NULL,
 junto VARCHAR(255) DEFAULT NULL,
 longitud VARCHAR(255) DEFAULT NULL,
 tipo INT(11) NOT NULL,
 numero INT(11) NOT NULL,
 PRIMARY KEY (datos_id)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Esta tabla esta para poder actualizarla con los procedimientos',
ROW_FORMAT = Compact,
TRANSACTIONAL = 0;
ALTER TABLE datos
 ADD INDEX numero2_index(numero2);
ALTER TABLE datos
 ADD INDEX numero_index(numero);
ALTER TABLE datos
 ADD INDEX tipo_index(tipo);


-- e1a Recibir un texto y un caracter, indicar si el caracter esta en el texto con LOCATE

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e1a(texto varchar(50), caracter varchar(1))
    BEGIN
      DECLARE pos int DEFAULT 0;
      DECLARE resul varchar (50) DEFAULT 0;
      SET pos=LOCATE(caracter,texto);
      IF pos=0 THEN
        SET resul='El caracter no está en el texto';
      ELSE
        SET resul='El caracter está en el texto';
      END IF;
      SELECT resul;
    END //
  DELIMITER ;

CALL e1a('hola','p');

-- e1b Recibir un texto y un caracter, indicar si el caracter esta en el texto con POSITION

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e1b(texto varchar(50), caracter varchar(1))
    BEGIN
      DECLARE pos int DEFAULT 0;
      DECLARE resul varchar (50) DEFAULT 0;
      SET pos=POSITION(caracter IN texto);
      IF pos=0 THEN
        SET resul='El caracter no está en el texto';
      ELSE
        SET resul='El caracter está en el texto';
      END IF;
      SELECT resul;
    END //
  DELIMITER ;

CALL e1b('hola','b');

-- e2 Recibir un texto y un caracter, mostrar el texto que haya antes de la primera vez que aparece el caracter

  DELIMITER //
  CREATE OR REPLACE PROCEDURE e2(texto varchar(50), caracter varchar(1))
    BEGIN
      DECLARE resul varchar (50) DEFAULT 0;
      SET resul=SUBSTRING_INDEX(texto,caracter,1);
      SELECT resul;
    END //
  DELIMITER ;
CALL e2('hola','o');

-- e3 procedimiento que recibe 3 numeros y tiene 2 argumentos de salida. Devolver el numero mas grande y el mas pequeño
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e3(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
    BEGIN
      SET mayor=GREATEST(n1,n2,n3);
      SET menor=LEAST(n1,n2,n3);       
    END //
  DELIMITER ;
  CALL e3(5,20,3,@mayor, @menor);
  SELECT @mayor,@menor;

-- e3b procedimiento que recibe 3 numeros y tiene 2 argumentos de salida. Devolver el numero mas grande y el mas pequeño. Con funcion IF
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e3b(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
    BEGIN
     set mayor=IF(n1>n2,IF(n1>n3,n1,n3),IF(n2>n3,n2,n3));
     SET menor=IF(n1<n2, IF(n1<n3,n1,n3),IF(n2<n3,n2,n3));
         
    END //
  DELIMITER ;
  CALL e3b(5,6,8,@mayor, @menor);
  SELECT @mayor,@menor;

 -- e4 Cuantos numeros1 y numeros2 de la tabla datos son mayores que 05
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e4()
    BEGIN
      DECLARE sum int DEFAULT 0;
      DECLARE num1 int DEFAULT 0;
      DECLARE num2 int DEFAULT 0;

      SELECT COUNT(*) INTO num1 FROM datos d WHERE numero1>50;      
      SELECT COUNT(*) INTO num2 FROM datos d WHERE numero2>50;
      set sum=num1+num2;
      SELECT SUM;
    END //
  DELIMITER ;

  CALL e4();

  -- e5 Calcular suma y resta de numero1 y numero2 de datos
    DELIMITER //
    CREATE OR REPLACE PROCEDURE e5()
      BEGIN
        UPDATE datos d
          SET d.suma=d.numero1+d.numero2;
        UPDATE datos d
          SET d.resta=d.numero1-d.numero2;
      END //
    DELIMITER ;
  
  CALL e5;

  -- e6 Suma y resta a NULL y calcular suma solo si numero1 es mayor que numero2, y la resta de numero2-numero1 si el 2 es mayor o numero1-numero2 si 1 es mayor
    DELIMITER //
    CREATE OR REPLACE PROCEDURE e6()
      BEGIN
        UPDATE datos d
          SET d.suma=NULL, d.resta=NULL;
        UPDATE datos d
          SET d.suma=numero1+numero2
          WHERE d.numero1>d.numero2;
        UPDATE datos d
          SET d.resta=d.numero1-d.numero2
          WHERE d.numero1>d.numero2;
        UPDATE datos d
          SET d.resta=d.numero2-d.numero1
          WHERE d.numero2>=d.numero1;
      END //
    DELIMITER;

  CALL e6;

  -- e7 Colocar en el campo junto el texto 1 y el texto 2
    DELIMITER //
    CREATE OR REPLACE PROCEDURE e7()
      BEGIN
        UPDATE datos d
          SET d.junto=CONCAT(texto1,' ',texto2);
      END //
    DELIMITER ;
    CALL e7;

   -- e8 Poner junto a NULL. Texto1-texto 2 en junto si rango es A, texto1+texto2 si es B, texto 1 si no es A o B
  DELIMITER //
  CREATE OR REPLACE PROCEDURE e8()
    BEGIN
      UPDATE datos d
        SET junto=NULL;
      UPDATE datos d
        set d.junto=IF(rango='A',CONCAT(d.texto1,'-',texto2),IF(rango='B',CONCAT(d.texto1,'+',d.texto2),texto1)) ;
    END //
  DELIMITER ;
  CALL e8;